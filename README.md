# Convertkit

Drupal Module for Convertkit using
their [API v3](https://developers.convertkit.com/).
Maintained and supported by
[Sujan Shrestha](https://www.drupal.org/sujan-shrestha).

## INTRODUCTION

This module uses
[v3](https://developers.convertkit.com/) of
the Convertkit api.
It allows users to add an API key, secret key and tag id in the settings.php
file (or via an admin UI). Then will allow users
to activate/enable a list and create a block
derivative for each list that is enabled.
It also provides an optional REST endpoint
to send signups to the CC API for enabled lists.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/convertkit_esp).

To submit bug reports and feature suggestions, or track changes
visit the [project issue page](https://www.drupal.org/project/issues/convertkit_esp).

## REQUIREMENTS

This module requires the following modules enabled in Drupal Core:

* Block
* REST (if wanting to use the provided REST endpoint)
* Webform (if wanting to send webform submissions to a enabled list)

This module also requires a
[Convertkit](https://app.convertkit.com) developer account
as well as an app created on the
[developer portal](https://app.convertkit.com/users/login).

## INSTALLATION

* Install as you would normally install a contributed Drupal module.
[Visit Drupal's documentation for further information](https://www.drupal.org/documentation/install/modules-themes/modules-7).

## CONFIGURATION

* Configure the user permissions in Administration » People » Permissions
* Create an app on Convertkit's
[developer portal](https://app.convertkit.com/users/login).
* Copy the API Key, secret key and tag id provided, add the key to your settings.php file.
* Your settings.php entry should look like this:

```php
  $settings['convertkit_esp'] = [
      'client_id' => 'your_apikey',
      'client_secret' => 'your_clientsecret',
      'tag_ids' => 'your_tag_id'
    ];
```

* If you prefer to use the UI, you can add these values to the admin UI
form located at `admin/config/services/convertkit`. Click save.

## WEBFORM HANDLER CONFIGURATION

* Go to Webforms list page (admin/structure/webform) and click "Edit" on
the desired Webform.
* Click Emails/Handlers secondary tab and then click on "Add handler" button.
* Click on "Add handler" button on "Convertkit" row.
* Fill in the form. You should have at least one tag id in your Convertkit
account, at least one tag id added in your Convertkit module settings
(/admin/config/services/convertkit), and at least one Email
field in your Webform.

