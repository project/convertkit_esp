<?php

namespace Drupal\convertkit_esp\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\convertkit_esp\Service\Convertkit;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ConvertkitConfig.
 *
 * Configuration form for adjusting content for the social feeds block.
 */
class ConvertkitConfig extends ConfigFormBase {

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   *   Messenger Interface.
   */
  protected $messenger;

  /**
   * Symfony\Component\HttpFoundation\RequestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\convertkit_esp\Service\Convertkit.
   *
   * @var \Drupal\convertkit_esp\Service\Convertkit
   *   Constant contact service.
   */
  protected $convertkit;

  /**
   * ConvertkitConfig constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal\Core\Config\ConfigFactoryInterface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Drupal\Core\Messenger\MessengerInterface.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Symfony\Component\HttpFoundation\RequestStack.
   * @param \Drupal\convertkit_esp\Service\Convertkit $convertkit
   *   Constant contact service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, RequestStack $request_stack, Convertkit $convertkit) {
    parent::__construct($config_factory);
    $this->messenger = $messenger;
    $this->requestStack = $request_stack;
    $this->convertkit = $convertkit;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('convertkit_esp')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'convertkit_esp_configure';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'convertkit_esp.config',
      'convertkit_esp.pkce',
      'convertkit_esp.tokens',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->convertkit->getConfig();
    $clientId = isset($settings['client_id']) ? $settings['client_id'] : NULL;
    $tagId = isset($settings['tag_ids']) ? $settings['tag_ids'] : NULL;

    $configType = isset($settings['config_type']) ? $settings['config_type'] : 'config';
    $secret = isset($settings['client_secret']) ? $settings['client_secret'] : NULL;
    $tokens = $this->config('convertkit_esp.tokens');
    $accessToken = isset($settings['access_token']) ? $settings['access_token'] : NULL;

    $form['auth'] = [
      '#type' => 'details',
      '#title' => $this->t('Authorization Settings'),
      '#collapsible' => TRUE,
      '#open' => (!$clientId || !$secret || !$tagId),
    ];

    $form['auth']['message'] = [
      '#markup' => $configType === 'settings.php' ? '<p>' . $this->t('<strong>NOTE:</strong> Application settings were found in your <strong>settings.php</strong> file. Please update information there or remove to use this form.') . '</p>' : '<p>' . $this->t('<strong>NOTE:</strong> Application settings are more secure when saved in your <strong>settings.php</strong> file. Please consider moving this information there. Example:') . '</p><pre>  $settings[\'convertkit_esp\'] = [
      \'client_id\' => \'your_apikey\',
      \'client_secret\' => \'your_clientsecret\',
      \'tag_ids\' => \'your_tag_id\'
    ];</pre>',
  ];

  $form['auth']['client_id'] = [
    '#type' => 'textfield',
    '#title' => $this->t('API Key'),
    '#default_value' => $clientId ? $clientId : NULL,
    '#required' => true,
    '#disabled' => $configType === 'settings.php' ? TRUE : FALSE
  ];

  $form['auth']['tag_ids'] = [
    '#type' => 'textfield',
    '#title' => $this->t('Tag Id'),
    '#default_value' => $tagId ? $tagId : '',
    '#required' => true,
    '#disabled' => $configType === 'settings.php' ? TRUE : FALSE,
    '#description' => $this->t('Select your applications authentication settings. See <a href="https://app.convertkit.com" target="_blank" rel="nofollow noreferrer">OAuth2 Flow Types</a> for more details. Note that PKCE Flow is not supported at this time. <a href="https://www.drupal.org/project/convertkit_esp/issues/3285446" target="_blank">See Issue #3285446 for details.</a>')
  ];

  $form['auth']['client_secret'] = [
    '#type' => 'textfield',
    '#title' => $this->t('Secret'),
    '#default_value' => $secret ? $secret : NULL,
    '#description' => $secret ? $this->t('Client Secret is for security purposes.') .  ($configType !== 'settings.php' ? $this->t('  Please re-enter the secret to update this value.') : '' ) : '',
    '#disabled' => $configType === 'settings.php' ? TRUE : FALSE,
    '#required' => true,
  ];

  $queryParams = [
    'client_id' => $clientId,
    'redirect_uri' => $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost() . '/admin/config/services/convertkit/callback',
    'response_type' => 'code',
    'state' => \Drupal::config('system.site')->get('uuid'),
    'scope' => 'offline_access'
  ];

  $form = parent::buildForm($form, $form_state);

  if ($configType === 'settings.php') {
    unset($form['actions']['submit']);
  }
  
  return $form;
}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('convertkit_esp.config');
    $tokens = $this->config('convertkit_esp.tokens');
    $clientId = $form_state->getValue('client_id');
    $secret = $form_state->getValue('client_secret');
    $tagId = $form_state->getValue('tag_ids');
    $config->clear('convertkit_esp.config');
    $config->set('client_id', $clientId);
    $config->set('client_secret', $secret);
    $config->set('tag_ids', $tagId);
    $config->save();
    $tokens->delete();

    $this->messenger->addMessage($this->t('Your configuration has been saved'));
  }

}
