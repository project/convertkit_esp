<?php

namespace Drupal\convertkit_esp\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\convertkit_esp\Service\Convertkit;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class ConvertkitBlockForm.
 *
 * Creates a form for block on frontend to post
 * contact info and send to Convertkit.
 */
class ConvertkitBlockForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  protected $formIdentifier;

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   *   Messenger Interface.
   */
  protected $messenger;

  /**
   * Drupal\convertkit_esp\Service\Convertkit.
   *
   * @var \Drupal\convertkit_esp\Service\Convertkit
   *   Constant contact service.
   */
  protected $convertkit;

  /**
   * \Drupal\Core\Extension\ModuleHandlerInterface
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   *   Module handler interface
   */
  protected $moduleHandler;

  /**
   * ConvertkitBlockForm constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   MessengerInterface.
   * @param \Drupal\convertkit_esp\Service\Convertkit $convertkit
   *   Constant contact service.
   */
  public function __construct(MessengerInterface $messenger, Convertkit $convertkit, ModuleHandlerInterface $moduleHandler) {
    $this->messenger = $messenger;
    $this->convertkit = $convertkit;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('convertkit_esp'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setFormIdentifier($formIdentifier) {
    $this->formIdentifier = $formIdentifier;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    $form_id = 'convertkit_esp_sigup_form';
    if ($this->formIdentifier) {
      $form_id .= '-' . $this->formIdentifier;
    }

    return $form_id;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $listConfig = []) {

    // Don't show anything if we don't have a tag_id set.
    if (!isset($listConfig['tag_ids'])) {
      return NULL;
    }

    if (isset($listConfig['success_message']) && $listConfig['success_message']) {
      $form_state->set('success_message', $listConfig['success_message']);
    }

    $form_state->set('tag_id', $listConfig['tag_id']);
    $form_state->set('tag_ids', $listConfig['tag_ids']);

    if (isset($listConfig['body']) && isset($listConfig['body']['value'])) {
      $form['body'] = [
        '#markup' => $listConfig['body']['value'],
      ];
    }

    $email_address_label = ($listConfig['email_address_label'] != '')?$listConfig['email_address_label']:'Email Address';
    $email_address_description = $listConfig['email_address_description']??'';
    $button_label = ($listConfig['button_label'] != '')?$listConfig['button_label']:'Subscribe';

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t($email_address_label),
      '#description' => ($email_address_description != '')?$this->t($email_address_description):'',
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t($button_label),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $message_type = 'status';

    $data = [
      'email_address' => $values['email'],
    ];

    // Add custom field values.
    // Skip adding it if there's no value.
    $fieldKeys = array_keys($values);
    foreach ($fieldKeys as $field) {
      if (strpos($field, 'custom_field__') !== false && isset($values[$field]) && $values[$field]) {
        $data['custom_fields'][str_replace('custom_field__', '', $field)] = $values[$field];
      }
    }

    $tag_ids = $form_state->get('tag_id');

    if($tag_ids == '') {
      $tag_ids = $form_state->get('tag_ids');
    }

    $tag_ids = (isset($tag_ids[0]))?$tag_ids[0]:'';

    $response = $this->convertkit->submitContactForm($data, $tag_ids);

    if (isset($response['error'])) {
      $message = 'There was a problem signing you up. Please try again later.';
      $message_type = 'error';
    }
    else {
      if ($form_state->get('success_message')) {
        $message = $form_state->get('success_message');
      }
      else {
        $message = $this->t('You have been signed up. Thank you.');
      }
    }

    $this->messenger->addMessage(strip_tags($message), $message_type);
  }

}
