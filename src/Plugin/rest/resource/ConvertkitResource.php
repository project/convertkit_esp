<?php

namespace Drupal\convertkit_esp\Plugin\rest\resource;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\convertkit_esp\Service\Convertkit;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ModifiedResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a Convertkit api Resource.
 *
 * @RestResource(
 *   id = "convertkit_esp_resource",
 *   label = @Translation("Convertkit Resource"),
 *   uri_paths = {
 *     "create" = "/convertkit/{tag_id}"
 *   }
 * )
 */
class ConvertkitResource extends ResourceBase {

  /**
   * Drupal\Core\Path\CurrentPathStack.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPath;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\convertkit_esp\Service\Convertkit.
   *
   * @var \Drupal\convertkit_esp\Service\Convertkit
   *   Constant contact service.
   */
  protected $convertkit;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, CurrentPathStack $current_path, ConfigFactoryInterface $config_factory, Convertkit $convertkit) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentPath = $current_path;
    $this->configFactory = $config_factory;
    $this->convertkit = $convertkit;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('convertkit_esp'),
      $container->get('path.current'),
      $container->get('config.factory'),
      $container->get('convertkit_esp')
    );
  }

  /**
   * Responds to entity POST requests.
   *
   * Takes the post request and sends it
   * to Convertkit api endpoints.
   * @param string $tag_id
   *   CC list (tag_id). Can be an array of list uuids.
   *
   * @param array $data
   *   Form data.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws HttpException in case of error.
   */
  public function post($tag_id, array $data) {
    $response = $this->convertkit->submitContactForm($data, [$tag_id]);
    return new ModifiedResourceResponse($response);
  }
}
