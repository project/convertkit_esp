<?php

namespace Drupal\convertkit_esp\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Serialization\Yaml;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformTokenManagerInterface;
use Drupal\convertkit_esp\Service\Convertkit;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Form submission to Convertkit handler.
 *
 * @WebformHandler(
 *   id = "convertkit",
 *   label = @Translation("Convertkit"),
 *   category = @Translation("Convertkit"),
 *   description = @Translation("Sends a form submission to a Convertkit API."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class WebformConvertkitHandler extends WebformHandlerBase {

    /**
   * Drupal\Core\Cache\CacheBackendInterface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   *   Drupal cache.
   */
    protected $cache;

    /**
   * Drupal\convertkit_esp\Service\Convertkit.
   *
   * @var \Drupal\convertkit_esp\Service\Convertkit
   *   Constant contact service.
   */
    protected $convertkit;

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $token_manager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->setCacheManager($container->get('cache.default'));
    $instance->setTokenManager($container->get('webform.token_manager'));
    $instance->setConvertkit($container->get('convertkit_esp'));
    return $instance;
  }

  /**
   * Set Cache dependency
   */
  protected function setCacheManager(CacheBackendInterface $cache) {
    $this->cache = $cache;
  }
  /**
   * Set Token Manager dependency
   */
  protected function setTokenManager(WebformTokenManagerInterface $token_manager) {
    $this->tokenManager = $token_manager;
  }

  /**
   * Set Convertkit dependency
   */
  protected function setConvertkit(Convertkit $convertkit) {
    $this->convertkit = $convertkit;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $fields = $this->getWebform()->getElementsInitializedAndFlattened();

    $configuration = $this->configuration;
    if(isset($configuration) && $configuration != '') {
      $tag_ids = $configuration['tag_id'];
    } else {
      $tag_ids = $this->convertkit->getTagLists();
    }

    $email_summary = $configuration['email'];
    if (!empty($fields[$configuration['email']])) {
      $email_summary = $fields[$configuration['email']]['#title'];
    }
    $email_summary = '<strong>' . $this->t('Email') . ': </strong>' . $email_summary;

    if(is_array($tag_ids)) {
      $tag_ids = implode(', ', $tag_ids);
    }

    $tag_ids = '<strong>' . $this->t('Tag Id') . ': </strong>' . $tag_ids;

    $markup = "$email_summary<br/>$tag_ids";
    return [
      '#markup' => $markup,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'tag_id' => '',
      'email' => ''
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $enabled = $this->configFactory->get('convertkit_esp.enabled_tag_ids')->getRawData();
    $options_data = $this->convertkit->getTagLists();

    if(!is_array($options_data)) {
      $options = array($options_data => $options_data);
    } else {
      foreach ($options_data as $key => $value) {
       $options[$value] = $value;
     }
   }

   $form['convertkit'] = [
    '#type' => 'fieldset',
    '#title' => $this->t('Convertkit settings'),
    '#attributes' => ['id' => 'webform-convertkit-handler-settings'],
  ];

  $form['convertkit']['update'] = [
    '#type' => 'submit',
    '#value' => $this->t('Refresh tag_ids & groups'),
    '#ajax' => [
      'callback' => [$this, 'ajaxConvertkitListHandler'],
      'wrapper' => 'webform-convertkit-handler-settings',
    ],
    '#submit' => [[get_class($this), 'convertkitUpdateConfigSubmit']],
  ];

  $form['convertkit']['tag_id'] = [
    '#type' => 'webform_select_other',
    '#title' => $this->t('Tag Id'),
    '#required' => TRUE,
    '#empty_option' => $this->t('- Select an option -'),
    '#default_value' => $this->configuration['tag_id'],
    '#options' => $options,
    '#ajax' => [
      'callback' => [$this, 'ajaxConvertkitListHandler'],
      'wrapper' => 'webform-convertkit-handler-settings',
    ],
    '#description' => $this->t('Select the tag_id you want to send this submission to. Alternatively, you can also use the Other field for token replacement.'),
  ];

  $fields = $this->getWebform()->getElementsInitializedAndFlattened();
  $options = [];
  foreach ($fields as $field_name => $field) {
    if (in_array($field['#type'], ['email', 'webform_email_confirm'])) {
      $options[$field_name] = $field['#title'];
    }
  }

  $default_value = $this->configuration['email'];
  if (empty($this->configuration['email']) && count($options) == 1) {
    $default_value = key($options);
  }
  $form['convertkit']['email'] = [
    '#type' => 'webform_select_other',
    '#title' => $this->t('Email field'),
    '#required' => TRUE,
    '#default_value' => $default_value,
    '#options' => $options,
    '#empty_option'=> $this->t('- Select an option -'),
    '#description' => $this->t('Select the email element you want to use for subscribing to the Convertkit tag_id specified above. Alternatively, you can also use the Other field for token replacement.'),
  ];

  $form['convertkit']['token_tree_link'] = $this->tokenManager->buildTreeLink();

  return $form;
}

  /**
   * Ajax callback to update Webform Convertkit settings.
   */
  public static function ajaxConvertkitListHandler(array $form, FormStateInterface $form_state) {
    return $form['settings']['convertkit'];
  }


  /**
   * Submit callback for the refresh button.
   */
  public static function convertkitUpdateConfigSubmit(array $form, FormStateInterface $form_state) {
    // Trigger tag_id and group category refetch by deleting tag_ids cache.
    $cache = \Drupal::cache();
    $cache->delete('convertkit_esp.tag_ids');
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();
    foreach ($this->configuration as $name => $value) {
      if (isset($values['convertkit'][$name])) {
        $this->configuration[$name] = $values['convertkit'][$name];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {

    // If update, do nothing
    if ($update) {
      return;
    }

    $fields = $webform_submission->toArray(TRUE);

    $configuration = $this->tokenManager->replace($this->configuration, $webform_submission);

    // Check if we are using a token for the tag_id field & replace value.
    if (isset($this->configuration['tag_id']) && strpos($this->configuration['tag_id'], '[webform_submission:values:') !== false) {
      $configuration['tag_id'] = null;
      $fieldToken = str_replace(['[webform_submission:values:', ']'], '', $this->configuration['tag_id']);

      if (isset($fields['data'][$fieldToken])) {
        if (is_string($fields['data'][$fieldToken])) {
          $configuration['tag_id'] = [$fields['data'][$fieldToken]];
        }
        else if (is_array($fields['data'][$fieldToken])) {
          $configuration['tag_id'] = $fields['data'][$fieldToken];
        }
      }
    }

    // Email could be a webform element or a string/token.
    if (!empty($fields['data'][$configuration['email']])) {
      $email = $fields['data'][$configuration['email']];
    }
    else {
      $email = $configuration['email'];
    }

    $handler_link = Link::createFromRoute(
      t('Edit handler'),
      'entity.webform.handler.edit_form',
      [
        'webform' => $this->getWebform()->id(),
        'webform_handler' => $this->getHandlerId(),
      ]
    )->toString();

    $submission_link = ($webform_submission->id()) ? $webform_submission->toLink($this->t('View'))->toString() : NULL;

    $context = [
      'link' => $submission_link . ' / ' . $handler_link,
      'webform_submission' => $webform_submission,
      'handler_id' => $this->getHandlerId(),
    ];

    if (!empty($configuration['tag_id']) && !empty($email)) {
      $data['email_address'] = $email;

      $this->convertkit->submitContactForm($data , $configuration['tag_id']);
    }
    else {
      if (empty($configuration['tag_id'])) {
        \Drupal::logger('webform_submission')->warning(
          'No Convertkit tag_id was provided to the handler.',
          $context
        );
      }
      if (empty($email)) {
        \Drupal::logger('webform_submission')->warning(
          'No email address was provided to the handler.',
          $context
        );
      }
    }
  }

}
