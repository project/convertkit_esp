<?php

namespace Drupal\convertkit_esp\Plugin\Field\FieldFormatter;

use Drupal\options\Plugin\Field\FieldFormatter\OptionsDefaultFormatter;

/**
 * Plugin implementation of the 'convertkit_lists_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "convertkit_lists_formatter",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "convertkit_lists",
 *   }
 * )
 */
class ConvertkitListDefaultFormatter extends OptionsDefaultFormatter {
}