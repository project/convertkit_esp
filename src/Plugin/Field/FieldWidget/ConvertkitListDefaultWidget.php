<?php

namespace Drupal\convertkit_esp\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;

/**
 * Plugin implementation of the 'convertkit_lists_default' widget.
 *
 * @FieldWidget(
 *   id = "convertkit_lists_default",
 *   label = @Translation("Select list"),
 *   field_types = {
 *     "convertkit_lists"
 *   },
 *   multiple_values = TRUE
 * )
 */
class ConvertkitListDefaultWidget extends OptionsSelectWidget {
}