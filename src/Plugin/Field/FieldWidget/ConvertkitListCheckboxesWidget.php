<?php

namespace Drupal\convertkit_esp\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget;

/**
 * Plugin implementation of the 'convertkit_lists_default' widget.
 *
 * @FieldWidget(
 *   id = "convertkit_lists_checkbox",
 *   label = @Translation("Check boxes/radio buttons"),
 *   field_types = {
 *     "convertkit_lists"
 *   },
 *   multiple_values = TRUE
 * )
 */
class ConvertkitListCheckBoxesWidget extends OptionsButtonsWidget {
}