<?php

namespace Drupal\convertkit_esp\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\OptGroup;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\OptionsProviderInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\options\Plugin\Field\FieldType\ListStringItem;

/**
 * Provides a field type for Convertkit Tags.
 * 
 * @FieldType(
 *   id = "convertkit_lists",
 *   label = @Translation("Convertkit Tags"),
 *   category = @Translation("Convertkit"),
 *   default_widget = "convertkit_lists_default",
 *   default_formatter = "convertkit_lists_formatter",
 * )
 */

class ConvertkitListItem extends FieldItemBase implements OptionsProviderInterface {

  /**
   * Instantiate our service.
   * Doesn'ts eem to be able to inject in FieldType
   * @see https://drupal.stackexchange.com/questions/224247/how-do-i-inject-a-dependency-into-a-fieldtype-plugin
   *
   * @return Drupal\convertkit_esp\Service\Convertkit;
   */
  private function convertkit() {
    return \Drupal::service('convertkit_esp');
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'field_mapping' => [
        'email_address' => NULL
      ]
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
      'indexes' => [
        'value' => ['value'],
      ],
    ];
  }

   /**
   * {@inheritdoc}
   */
   public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
    ->setLabel(new TranslatableMarkup('List UUID'))
    ->addConstraint('Length', ['max' => 255]);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, false);
    $entityType = $this->getEntity();
    $entityFields = $entityType->getFields();

    $element['field_mapping'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Entity Field Mapping to Convertkit Fields'),
      '#tree' => TRUE,
      '#states' => [
        'visible' => [':input[name="settings[subscribe_on_save]"]' => ['checked' => TRUE]]
      ]
    ];

    $element['field_mapping']['email_address'] = [
      '#type' => 'select',
      '#title' => $this->t('Email'),
      '#default_value' => isset($this->getSetting('field_mapping')['email_address']) ? $this->getSetting('field_mapping')['email_address'] : NULL,
      '#description' => $this->t('Requires a field of type <strong>email</strong>'),
      '#states' => [
        'required' => [':input[name="settings[subscribe_on_save]"]' => ['checked' => TRUE]]
      ]
    ];

    // Add field mapping options
    // @TODO - what to do if field is deleted.
    foreach ($entityFields as $fieldName => $fieldItemList) {
      $fieldDefinition = $fieldItemList->getFieldDefinition();
      $fieldLabel = $fieldDefinition->getLabel();
      $fieldType = $fieldDefinition->getType();

      if ($fieldType === 'email') {
        $element['field_mapping']['email_address']['#options'][$fieldName] = $fieldLabel;
      }
    }

    return $element;
  }


  /**
   * {@inheritdoc}
   */
  public function getPossibleValues(AccountInterface $account = NULL) {
    return $this->getSettableValues($account);
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleOptions(AccountInterface $account = NULL) {
    return $this->getSettableOptions($account);
  }


  /**
   * {@inheritdoc}
   */
  public function getSettableValues(AccountInterface $account = NULL) {
    // Flatten options first, because "settable options" may contain group
    // arrays.
    $flatten_options = OptGroup::flattenOptions($this->getSettableOptions($account));
    return array_keys($flatten_options);
  }

  public function getSettableOptions(AccountInterface $account = NULL) {
    $cc = $this->convertkit();
    $options = [];

    $lists = $cc->getEnabledContactLists();

    foreach ($lists as $id => $list) {
      $options[$list] = $list;
    }
    
    return $options;
  }
}