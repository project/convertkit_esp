<?php

namespace Drupal\convertkit_esp\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\convertkit_esp\Service\Convertkit;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides block plugin definitions for Convertkit blocks.
 *
 * @see \Drupal\convertkit_esp\Plugin\Block\ConvertkitBlock
 */
class ConvertkitBlockDerivative extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\convertkit_esp\Service\Convertkit.
   *
   * @var \Drupal\convertkit_esp\Service\Convertkit
   *   Constant contact service.
   */
  protected $convertkit;

  /**
   * ConvertkitBlockDerivative constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   ConfigFactoryInterface.
   * @param \Drupal\convertkit_esp\Service\Convertkit $convertkit
   *   Constant contact service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, Convertkit $convertkit) {
    $this->configFactory = $configFactory;
    $this->convertkit = $convertkit;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('config.factory'),
      $container->get('convertkit_esp')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $lists = $this->convertkit->getEnabledContactLists(false);

    return $this->derivatives;
  }

}
