<?php

namespace Drupal\convertkit_esp\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\convertkit_esp\Service\Convertkit;

/**
 * Provides a convertkit signup form per list that is enabled.
 *
 * @Block(
 *   id = "convertkit_esp",
 *   admin_label = @Translation("Convertkit Signup Form"),
 *   deriver = "Drupal\convertkit_esp\Plugin\Derivative\ConvertkitBlockDerivative"
 * )
 */
class ConvertkitBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Form\FormBuilderInterface.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Drupal\convertkit_esp\Service\Convertkit.
   *
   * @var \Drupal\convertkit_esp\Service\Convertkit
   *   Constant contact service.
   */
  protected $convertkit;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder, Convertkit $convertkit) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->convertkit = $convertkit;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('convertkit_esp')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $ccConfig = $this->convertkit->getConfig();

    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#default_value' => isset($config['body']) ? $config['body']['value'] : NULL,
      '#format' => isset($config['format']) ? $config['body']['format'] : NULL,
    ];
    $form['success_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom Success Message'),
      '#default_value' => isset($config['success_message']) ? $config['success_message'] : NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();
    $ccConfig = $this->convertkit->getConfig();

    $this->configuration['body'] = $values['body'];
    $this->configuration['success_message'] = $values['success_message'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $listConfig = $this->getConfiguration();
    $listConfig['tag_id'] = str_replace('convertkit_esp:', '', $this->getPluginId());

    return $this->formBuilder->getForm('Drupal\convertkit_esp\Form\ConvertkitBlockForm', $listConfig);
  }

}
