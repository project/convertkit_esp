<?php

namespace Drupal\convertkit_esp\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\convertkit_esp\Service\Convertkit;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;

/**
 * Provides a convertkit signup form per list that is enabled.
 *
 * @Block(
 *   id = "convertkit_esp_multi",
 *   admin_label = @Translation("Convertkit Subscription Signup Form"),
 * )
 */
class ConvertkitMultiBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Form\FormBuilderInterface.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder, Convertkit $convertkit, ConfigFactoryInterface $config) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->convertkit = $convertkit;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('convertkit_esp'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $ccConfig = $this->convertkit->getConfig();
    $customFields = $this->convertkit->getCustomFields();
    $enabled = $this->config->get('convertkit_esp.enabled_lists')->getRawData();

    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#default_value' => isset($config['body']) ? $config['body']['value'] : NULL,
      '#format' => isset($config['format']) ? $config['body']['format'] : NULL,
    ];
    $form['success_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom Success Message'),
      '#default_value' => isset($config['success_message']) ? $config['success_message'] : NULL,
    ];

    $form['email_address_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label for email address'),
      '#default_value' => isset($config['email_address_label']) ? $config['email_address_label'] : 'Email:',
    ];

    $form['button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label for button'),
      '#default_value' => isset($config['button_label']) ? $config['button_label'] : 'Subscribe',
    ];

    $form['email_address_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description for email address'),
      '#default_value' => isset($config['email_address_description']) ? $config['email_address_description'] : 'Subscribe us for the more details',
    ];

    if (isset($ccConfig['client_secret']) && $ccConfig['client_secret']) {
      $form['tag_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Tag Id'),
        '#default_value' => isset($config['tag_id']) ? $config['tag_id'] : $ccConfig['tag_ids'],
      ];
    } else {
      $url_object = Url::fromRoute('convertkit_esp.config');
      $form['help'] = [
        '#type' => 'link',
        '#url' => $url_object,
        '#title' => $this->t('Please enter the convertkit api details to add the form.'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();

    $this->configuration['body'] = $values['body'];
    $this->configuration['success_message'] = $values['success_message'];
    $this->configuration['button_label'] = $values['button_label'];
    $this->configuration['email_address_label'] = $values['email_address_label'];
    $this->configuration['email_address_description'] = $values['email_address_description'];
    $this->configuration['tag_id'] = $values['tag_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $listConfig = $this->getConfiguration();

    $listConfig['tag_ids'] = $this->convertkit->getTagLists();

    return $this->formBuilder->getForm('Drupal\convertkit_esp\Form\ConvertkitBlockForm', $listConfig);
  }

}
